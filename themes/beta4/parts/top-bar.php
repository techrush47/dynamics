<div class="top-bar-container show-for-medium-up">
    <div class="sticky">
        <nav class="top-bar fixed" data-topbar role="navigation">
            <div class="">
                <ul class="title-area">
                    <li class="name">
                        <h1><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="logo"  /></a></h1>
                    </li>
                </ul>
            <ul class="main-nav">
                <li><a href="<?php echo site_url(); ?>/#home">HOME</a></li>
                <li><a href="<?php echo site_url(); ?>/#ourhistory">ABOUT</a></li>
                <li><a href="<?php echo site_url(); ?>/#services">SERVICES</a></li>                    
                <li><a href="<?php echo site_url(); ?>/#team">OUR TEAM</a></li>
                <li><a href="<?php echo site_url(); ?>/#contact">UPLOAD RESUME</a></li>
                <li><a href="<?php echo site_url(); ?>/jobs">SEARCH JOBS</li>                    
                <li><a href="<?php echo site_url(); ?>/blog">BLOG</a></li>
                <li><a href="<?php echo site_url(); ?>/#contact">CONTACT</a></li>
            </ul>
            </div>
            <section class="top-bar-section">
                <?php foundationPress_top_bar_l(); ?>
                <?php foundationPress_top_bar_r(); ?>
            </section>

        </nav>
    </div>
</div>
