</section>
<footer class="main-bottom">
	<footer class="row">
		<h4>Dynamics Associates</h4>
		<p>Dynamics Associates is an executive search firm located in New York City. Our goal is to help our clients receive top executive employees while also helping 
			our candidates enhance the future success of their careers.  </p>
		 <p class="address">515 Madison Avenue #18a <br /> New York, NY 10022 <br /> (212) 629-8655</p>
		<?php do_action('foundationPress_before_footer'); ?>
		<a target="_blank" vhref="https://www.facebook.com/DynamicsNY?ref=hl"><img src="<?php bloginfo('template_directory'); ?>/assets/img/footer-facebook.png" alt="facebook"  /></a>
		<a target="_blank" href="https://twitter.com/DynamicsJobsNY"><a href="#"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/img/footer-twitter.png" alt="twitter"  /></a>
		<a target="_blank" href="#"><a href="https://www.linkedin.com/company/58650?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A1-1-1%2CtarId%3A1427398569981%2Ctas%3Adynamics+ass"><img src="<?php bloginfo('template_directory'); ?>/assets/img/footer-linkedin.png" alt="linkedin"  /></a>
		<a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/img/footer-email.png" alt="email"  /></a>

		<?php do_action('foundationPress_after_footer'); ?>
	</footer>
	<a class="exit-off-canvas"></a>
</footer>

	<?php do_action('foundationPress_layout_end'); ?>
	</div>
</div>

<?php wp_footer(); ?>
<?php do_action('foundationPress_before_closing_body'); ?>
<script>
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({scrollTop: (($(target).offset().top)-100)
        }, 500);
        return false;
      }
    }
  });
});
</script>

</body>
</html>
