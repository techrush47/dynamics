<?php
/*
Template Name: Hero
*/
get_header(); ?>

<div class="hero-slider" id="home">
	<div id="home-slide-1" class="home-slide">
		<img src="<?php bloginfo('template_directory'); ?>/assets/img/slide-bull.jpg" alt="wall street bull"  />
		<div class=" slide-text-wrap" >
	        <p class="home-tagline">Financial Recruiting Experts</p>
	        <p class="home-subtext">Our team values the rewarding results of building meaningful relationships to help candidates
	         enhance the future of their careers.  For us, it’s about more than placing candidates. Our goal is to identify 
	         top-tier professionals and offer them life changing opportunities. </p>
	         <a class="button home-button" href="#services">OUR SERVICES</a>
	    </div>
	</div>
	<div id="home-slide-2" class="home-slide" >
		<img src="<?php bloginfo('template_directory'); ?>/assets/img/wallst-slide.jpg" alt="wall street"  />
		<div class="slide-text-wrap2" >
	        <p class="home-tagline">Finding You The Right Position</p>
	        <p class="home-subtext">Out experiences team is here to find you the right position in the finanical services industry.</p>
	        <a class="button home-button" href="#contact">UPLOAD RESUME</a>
	        <a class="button home-button" href="#contact" style="margin-left: 5%;">SEARCH JOBS</a>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.hero-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000
		});
	});
</script>

<section id="home-buildings" role="banner">
	<div class="row">
       <p class="home-buildings">With over 25 years of experiences in financial services recruiting, Dynamics Associates
        can help you land the perfect job to enhance your career.</p>
        <a href="#our-process" class="button home-button" style="margin-right: -4%;">OUR PROCESS</a>
        <a href="#contact" class="button home-button" style="margin-left: 18%;" >UPLOAD RESUME</a>
	</div>

</section>

<section class="our-history">
	<div id="ourhistory" ></div>
	<div class="row">
		<h2>Our History</h2>

		<p>Dynamics Associates was founded in 1987 by President and CEO, Scott Grossman. The firm was established in order
		 to help clients in the financial services industry find top-tier candidates while also providing candidates with
		  challenging and rewarding opportunities. We pride ourselves in being able to successfully have our candidates
		   advance their careers within our clients’ organizations. Our exceptional performance, along with our
		    candidates’ career advancement, have allowed us to enjoy a proven history of success. In recent years,
		     Dynamics established an alternative investments division. The need for this division was a direct result
		      of the rapid growth of accounting, operations, and compliance positions within the alternative investments
		       space (hedge funds, private equity funds, fund of funds, etc.). We excel in establishing personal 
		       relationships with all of our candidates. Our convenient location gives us a unique ability to properly
		        assess the skills and qualifications of each potential candidate by personally meeting each candidate
		         we submit to our clients.</p>


	</div>

	<div class="home-divider" ></div>

</section>
<section class="services" id="services">

	<div class="row">
		<h2>PROVIDING SERVICE FOR ALL OF YOUR NEEDS</h2>
		<div class="service-slider">

				<div class="service-slide">
					<h3 class="service-header">EMPLOYER SERVICES</h3>
					<p>As a firm with many years of experience, we understand the importance of establishing a solid and reliable
					 team. Our goal is to offer you, our clients, some of the most successful and best fit candidates to fulfill
					  all of your hiring needs.</p> 
				</div>
				<div class="service-slide-mid">
					<h3 class="service-header">EXECUTIVE PLACEMENT</h3>
					<p>Our team of recruiters have experience and market expertise which allows them to match our candidates with
					 the best opportunities for their career advancement.</p>		
					</div>
				<div class="service-slide">
					<h3 class="service-header">CAREER OPPORTUNITIES</h3>
					<p>Our firm’s reputation and our industry relationships allow us to provide our candidates with some of the 
						top searches on the market.</p>
				</div>

			</div>

	</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.service-slider').slick({
 		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		centerMode: true,
		 arrows: false,
		autoplaySpeed: 4000
		});
	});
</script>


</section>
<section class="areas">
	<div class="row">
	<h4>Areas Of Expertise</h4>
	<p>Dynamics Associates works closely with some of the most prestigious buy and sell side firms on Wall Street. We partner
	 with our clients to ensure that they receive the highest quality candidates who can match their employment needs. We 
	 recruit for positions in the following areas: </p>
		 <div id="expert-wrap">
			<ul>
				<li>Internal Audit</li>
				<li>Risk  Management</li>

			</ul>	 
			<ul>
				<li>Fund Accounting </li>
				<li>Corporate Accounting </li>
				<li>Middle Office</li>
			</ul>	
			<ul>
				<li>Treasury </li>
				<li>Operations </li>
				<li>Valuations</li>

			</ul>	
			<ul>
				<li>Financial Accounting </li>
				<li>Regulatory Reporting </li>
			</ul>
		</div>	
	</div>
</section>




<section id="our-process" class="our-process">
	<div class="row">
		<h3>Our Process</h3>
		<div id="process-wrapper">
			<div class="large-2 columns process" style="margin-top: -9px;" >
				<h6>Step 1</h6>
					<p>We begin by identifying client needs. We conduct in-depth candidate research to find 
						a matching set of candidates to match these needs.  </p>
			</div>
			<div class="large-2 columns process">
				<h6>Step 2</h6>
					<p>Phone screens help us gain an in-depth understanding about the candidate’s desires and
					 professional experience. </p>
			</div>
			<div class="large-2 columns process">
				<h6>Step 3</h6>
					<p>Candidates are interviewed in-person, where they meet face-to-face with a recruiter</p>
			</div>
			<div class="large-2 columns process">
				<h6>Step 4</h6>
					<p>Recruiters set up external interviews at the client companies whose open roles best match 
						the candidate’s qualifications. </p>
			</div>
			<div class="large-2 columns process">
				<h6>Step 5</h6>
					<p>Once candidates are matched with a client company, they are placed at the perfect position
						that will help enrich their career. </p>
			</div>
			<div class="large-2 columns process" style="float: left;">
				<h6>Step 6</h6>
					<p>We maintain relationships with our candidates throughout their entire careers and ensure
					 that their goals are being achieved. </p>
			</div>
		</div>
        <p class="button  process-button" style="left: 460px;" >SEARCH JOBS</p>
        <p class="button  process-button" style="right: 460px;">UPLOAD RESUME</p>
	</div>
</section>
<section class="team">
	<div class="row">
		<h3 id="team">Meet Our Team</h3>
			<div class="large-4 columns gallery-item">
				<img class="" src="<?php bloginfo('template_directory'); ?>/assets/img/Brandon-Doohan.jpg" alt="brandon doohan"  />
				<div class="contenthover">
					<p>After graduating from Siena College with a BS in Marketing Management, Brandon began his career in financial operations
					 working for the Rockefeller Family. In 2007, he moved into executive recruiting where he built a brand for himself by
					  expanding his client base within the industry. As the Managing Director, he helps run the finance and accounting practice.</p>
				</div>
				<p class="name">Brandon Doohan </p>
				<p class="title">Managing Director </p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>
			<div class="large-4 columns gallery-item">
				<img class="d2" src="<?php bloginfo('template_directory'); ?>/assets/img/Scott-Grossman.jpg" alt="Scott Grossman"  />
				<div class="contenthover">
					<p>Scott founded Dynamics Associates in 1987. His vision was to establish a recruiting firm that would achieve more
					 than just placing candidates. He desired to create a company that would identify top-tier professionals in the
					  financial services industry and offer them challenging and rewarding opportunities. Over the past 28 years, he has
					  played a vital role in helping the firm expand to the success it has become today.  </p>
				</div>
				<p class="name">Scott Grossman</p>
				<p class="title">President and CEO</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>
			<div class="large-4 columns  gallery-item">
				<img class="d2" src="<?php bloginfo('template_directory'); ?>/assets/img/Sal-Khan.jpg" alt="Sal Khan"  />
				<div class="contenthover ">
					<p>Sal has been with Dynamics since the fall of 1996. Having spent his entire career with Dynamics, Sal has been a 
						staple of the firm’s success and growth within various disciplines. Since joining Dynamics, he has been able to 
						expand the firm’s financial services practice with a focus on internal audit, risk management, valuations and 
						controlling. Sal has been able to build and maintain some of Dynamics’ most prestigious clients during his tenure</p>
				</div>
				<p class="name">Sal Khan</p>
				<p class="title">Managing Director </p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns gallery-item">
				<img  class="d2" src="<?php bloginfo('template_directory'); ?>/assets/img/Dawn.jpg" alt="team member dawn"  />
				<div class="contenthover small-content">
					<p>With over ten years of experience in both mortgage derivative sales and recruiting,
					 Dawn is able to use her expertise to lead the alternative investments division at our firm. Outside of Dynamics, 
					 Dawn is also a world traveler, marathon runner and yoga teacher.  </p>
				</div>
				<p class="name">Dawn Magnotta</p>
				<p class="title">Head of Alternative Investments </p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns gallery-item">
				<img class="d2" src="<?php bloginfo('template_directory'); ?>/assets/img/Lisa.jpg" alt="team member"  />
				<div class="contenthover small-content">
					<p>Lisa serves as a career coach with a focus on hiring and placing for the Investment Banking Industry
					 as well as the Alternative Investment space. Lisa is also fluent in Russian and is a member of the National Association
					  of Professional Women.</p>
				</div>
				<p class="name">Lisa Mogilner</p>
				<p class="title">Senior Executive Recruiter</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>

				</div>
			</div>

			<div class="large-2 columns">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Thomas.jpg" alt="team member"  />
				<p class="name">Thomas Migliore</p>
				<p class="title">Senior Executive Recruiter</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Angela.jpg" alt="team member"  />
				<p class="name">Angela Batra</p>
				<p class="title">Executive Recruiter</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Doug.jpg" alt="team member"  />
				<p class="name">Doug Smith</p>
				<p class="title">Senior Executive Search Consultant </p>
								<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns" style="margin-left: 10%;">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Michael.jpg" alt="team member"  />
				<p class="name">Michael Cracchiola</p>
				<p class="title">Executive Recruiter</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Glenna.jpg" alt="team member"  />
				<p class="name">Glenna Weisenfeld</p>
				<p class="title">Executive Recruiter</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Angela.jpg" alt="team member"  />
				<p class="name">Angela Verdirame</p>
				<p class="title">Human Capital Manager</p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/Francesca.jpg" alt="team member"  />
				<p class="name">Francesca LoGiudice </p>
				<p class="title">Marketing Director </p>
				<div class="team-contact">
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/linkedin-lg.jpg" alt="linked in"  /> LinkedIn</a>
					<a href=""><img src="<?php bloginfo('template_directory'); ?>/assets/img/email-lg.jpg" alt="email"  /> Email</a>
				</div>
			</div>

			<div class="large-2 columns">

			</div>
	</div>

</section>

	<div class="home-divider"></div>
<section class="join-team">
	<div class="row">
		<div class="join-wrap">
			<h3>Want to join the Dynamics team?</h3>
			<p>Send your resume to one of the following available positions: </p>
			<a class="button home-button">EXECUTIVE RECRUITER</a>
			<a class="button home-button">INTERNSHIP</a>
		</div>
	</div>
</section>
<section class="contact">
	<div class="row">
		<h1 class="entry-title" id="contact">Contact</h1>
		<p>Send us your information so that we can have a more in-depth understanding <br /> of
			your background and the opportunities you would be a match for.</p>
		<form style="width: 500px;"> 
			<div class="row"> 
				<div class="large-12 columns"> 
					<label>Full Name<input type="text" placeholder="" /> </label>
				</div> 
			</div>

			<div class="row"> 
				<div class="large-6 columns"> <label>Email Address<input type="text" placeholder="" /> </label> 
				</div> 
				<div class="large-6 columns"> <label>Phone Number<input type="text" placeholder="" /> </label>
				 </div>
			</div>
			<div class="row"> 
				<div class="large-6 columns"> <label>Upload Resume<input type="file" placeholder="" /> </label> 
				</div> 
				<div class="large-6 columns"> <label>LinkedIn<input type="text" placeholder="" /> </label>
				 </div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<label>Message</label>
					<textarea></textarea>
				</div>
			</div>

			<div class="row"> 
				<div class="large-6 columns"><label>Job Code</label><input style="background-color: #455C6A;"type="text" placeholder="" /> 
				</div> 
			</div>

			<div class="row">
				<div class="large-4 columns">
					<button class="home-button process-button">SUBMIT</button>
				</div>
			</div>

		</form>


<!--		<div class="small-12 large-8 columns" role="main">

		<?php //do_action('foundationPress_before_content'); ?>

		<?php// while (have_posts()) : the_post(); ?>
			<article <?php p//ost_class() ?> id="post-<?php the_ID(); ?>">
				<header>

				</header>
				<?php //do_action('foundationPress_page_before_entry_content'); ?>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				<footer>
					<?php //wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'FoundationPress'), 'after' => '</p></nav>' )); ?>
				</footer>
				<?php// do_action('foundationPress_page_before_comments'); ?>

				<?php //do_action('foundationPress_page_after_comments'); ?>
			</article>
		<?php //endwhile;?>

		<?php //do_action('foundationPress_after_content'); ?>

		</div>-->

</div>
</section>
<?php get_footer(); ?>
