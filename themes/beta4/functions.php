<?php
/*
Author: Ole Fredrik Lie
URL: http://olefredrik.com
*/


// Various clean up functions
require_once('library/cleanup.php');

// Required for Foundation to work properly
require_once('library/foundation.php');

// Register all navigation menus
require_once('library/navigation.php');

// Add menu walker
require_once('library/menu-walker.php');

// Create widget areas in sidebar and footer
require_once('library/widget-areas.php');

// Return entry meta information for posts
require_once('library/entry-meta.php');

// Enqueue scripts
require_once('library/enqueue-scripts.php');

// Add theme support
require_once('library/theme-support.php');

// Add Header image
require_once('library/custom-header.php');

function pr_scripts_styles() {

    wp_enqueue_script('jquery'); // just enqueue as its already registered 
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );

    /*   REGISTER ALL JS FOR SITE */

    wp_register_script('slick',get_stylesheet_directory_uri().'/js/vendor/slick.min.js');
    wp_register_script('smooth',get_stylesheet_directory_uri().'/js/vendor/jquery.smooth-scroll.min.js');

    /*   CALL ALL CSS AND SCRIPTS FOR SITE */
    wp_enqueue_script('jquery');
    wp_enqueue_script('slick');
    wp_enqueue_script('smooth');


}
add_action( 'wp_enqueue_scripts', 'pr_scripts_styles' );

if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 180, 180, true ); // default Post Thumbnail dimensions (cropped)

// additional image sizes
// delete the next line if you do not need additional image sizes
add_image_size( 'post-image', 950, 700, true ); //300 pixels wide (and unlimited height)
}

?>