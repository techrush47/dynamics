<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php if ( is_category() ) {
			echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
		} elseif ( is_tag() ) {
			echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' ); 
		} elseif ( is_archive() ) {
			wp_title(''); echo ' Archive | '; bloginfo( 'name' );
		} elseif ( is_search() ) {
			echo 'Search for &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
		} elseif ( is_home() || is_front_page() ) {
			bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
		}  elseif ( is_404() ) {
			echo 'Error 404 Not Found | '; bloginfo( 'name' );
		} elseif ( is_single() ) {
			wp_title('');
		} else {
			echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
		} ?></title>
		
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/css/foundation.css" />
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ; ?>/style.css" />
		<link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri() ; ?>/assets/img/icons/apple-touch-icon-precomposed.png">


		<?php wp_head(); ?>

<script>
        $(document).ready(
          function() {
              // initial state
              $(".process:first").css("background-color", "#fff");
              $(".process:first").css("background-image", "none");
              $(".process:first").css("height", "360px");
              $(".process:first").css("color", "#064771");
              $(".process:first").css("font-size", "16px");

 
              // hover state
				$(".process:first").hover(function() {
				$(".process:first").css("background-color", "#fff");
				$(".process:first").css("background-image", "none");
				$(".process:first").css("color", "#064771");
				$(".process:first").css("font-size", "16px");
				$(".process:first").css("height", "360px");
              });
 
              // de-emphasize when hovering others (loop)
              var x = $(".process").length;
 
              for (i = 1; i <= x; i++) {
                 $(".process").eq(i).mouseover(function() {
                     $(".process:first").css("background-color", "#6f7074");
                     $(".process:first").css("color", "#fff");
                     $(".process:first").css("font-size", "13px");
					 $(".process:first").css("height", "340px");
                     $(".process:first").css("transition", "All 1s ease");
                     $(".process:first").css("-webkit-transition", "All .7s ease");
                     $(".process:first").css("-moz-transition", "All .7s ease");
                     $(".process:first").css("-o-transition", "All .7s ease");
                     $(".process:first").css("margin", "10px");
                 });
 
					$(".process").eq(i).mouseout(function() {
					$(".process:first").css("background-color", "#fff");
					$(".process:first").css("color", "#064771");
					$(".process:first").css("font-size", "16px");
					$(".process:first").css("height", "340px");




                 });
             }
         });
   </script>
<script>
$(document).ready( function() {

    $('.gallery-item').hover( function() {
        $(this).find('.contenthover').fadeIn(400);
    }, function() {
        $(this).find('.contenthover').fadeOut(400);
    });
	
});
</script>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action('foundationPress_after_body'); ?>
	
	<div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
	
	<?php do_action('foundationPress_layout_start'); ?>
	
	<nav class="tab-bar show-for-small-only">
		<section class="left-small">
			<a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
		</section>
		<section class="middle tab-bar-section">
			
			<h1 class="title"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="logo"  /></a></h1>

		</section>
	</nav>

	<?php get_template_part('parts/off-canvas-menu'); ?>

	<?php get_template_part('parts/top-bar'); ?>

<section class="container scrollme" role="document">
	<?php do_action('foundationPress_after_header'); ?>